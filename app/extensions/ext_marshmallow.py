# ext_marshmallow.py - Extends `marshmallow.` to suit app-specific needs.
# Copyright 2016 Charles Jordan Cabero
#
# This file is part of flask-starter-kit.
#
# flask-starter-kit is distributed under the 3-clause BSD license,
# for more information, see LICENSE.
from webargs import fields


class TupleField(fields.Field):
    def _deserialize(self, value, attr, data):
        return value

    def _serialize(self, value, attr, obj):
        return value
