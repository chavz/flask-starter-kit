from flask.ext.sqlalchemy import SQLAlchemy, SignallingSession, SessionBase


class SessionWithBinds(SignallingSession):
    """This extends the flask-sqlalchemy signalling session so that we may
    provide our own 'binds' argument.

    See https://github.com/mitsuhiko/flask-sqlalchemy/pull/168
    Also http://stackoverflow.com/a/26624146/2475170

    """
    def __init__(self, db, autocommit=False, autoflush=True, **options):
        #: The application that this session belongs to.
        self.app = db.get_app()
        self._model_changes = {}
        #: A flag that controls whether this session should keep track of
        #: model modifications.  The default value for this attribute
        #: is set from the ``SQLALCHEMY_TRACK_MODIFICATIONS`` config
        #: key.
        self.emit_modification_signals = \
            self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS']
        bind = options.pop('bind', None) or db.engine

        # Our changes to allow a 'binds' argument
        binds = options.pop('binds', None)
        if binds is None:
            binds = db.get_binds(self.app)

        SessionBase.__init__(
            self, autocommit=autocommit, autoflush=autoflush,
            bind=bind, binds=binds, **options
        )


class TestFriendlySQLAlchemy(SQLAlchemy):
    """For overriding create_session to return our own Session class"""
    def create_session(self, options):
        return SessionWithBinds(self, **options)
