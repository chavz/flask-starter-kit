# ext_webargs.py - Extends `webargs` to suit application-specific needs.
# Copyright 2016 Charles Jordan Cabero
#
# This file is part of flask-starter-kit.
#
# flask-starter-kit is distributed under the 3-clause BSD license,
# for more information, see LICENSE.

"""Contains extensions to `webargs` to suit application-specific needs."""

from functools import partial
from ast import literal_eval

import marshmallow as ma
import marshmallow_jsonapi as majapi
import webargs.core
import webargs.flaskparser

from app import db
from app.helpers import get_related_model, is_like_list, is_relationship

# functions `parse_sparse_fields`, `parse_sorts`, & `parse_filters`
# internal code was derived from Flask-Restless tailored to app needs,
# Copyright 2011 Lincoln de Sousa, Copyright 2012 Jeffrey Finkelstein.
# Flask-Restless is distributed under the 3-clause BSD license.


#: The query parameter key that identifies sort fields
#: in a :http:method:`get` request.
SORT_PARAM = 'sort'

#: The query parameter key that identifies filter objects in a
#: :http:method:`get` request.
FILTER_PARAM = 'filter[objects]'

#: The query parameter key that signifies fuzzy filtering in a
#: :http:method:`get` request.
FILTER_FUZZY_PARAM = 'filter[fuzzy]'


def parse_sparse_fields(request):
    fields = dict((key[7:-1], set(value.split(',')))
                  for key, value in request.args.items()
                  if key.startswith('fields[') and key.endswith(']'))
    return fields


def parse_sorts(request):
    sort = request.args.get(SORT_PARAM)
    if not sort:
        sort = []
    else:
        sort = [
            (value[:1], value[1:])
            if value.startswith('-') or value.startswith('+')
            else ('+', value)
            for value in sort.split(',')]
    return sort


def parse_filters(request, schema):
    # get the model from the schema
    pri_model = db.Model._decl_class_registry.get(
        getattr(schema.Meta, 'model_', None), None)

    def format_filter(model, field, val):
        # For deep level fields, extract only top most field
        # and group the rest of it to another string for further
        # extraction later on in the recursive
        field = field.split('.', 1)

        # To make sure we always get a two item list from the split
        # we always pad the list to it's supposed length of (2)
        # and fill-up any missing values with None
        field, rf = field + [None] * (2 - len(field))

        # If the field is a relationship, use the `has` or `any` operator
        # then continue to recursively format for `val`, passing the
        # related model and the appropriate field. Otherwise, the field
        # is an attribute, so we use the `like` operator directly.
        if model is not None and is_relationship(model, field):
            related_model = get_related_model(model, field)
            field_name = rf or \
                getattr(related_model, '__pri_search_attr__', 'id')
            new_filter = {
                'name': field,
                'op': 'any' if is_like_list(model, field) else 'has',
                'val': format_filter(related_model, field_name, val)}
        else:
            new_filter = {'name': field, 'op': 'like', 'val': '%' + val + '%'}
        return new_filter

    # fetch pre-formatted filters if there's any
    filters = literal_eval(request.args.get(FILTER_PARAM, '[]'))

    # parse for those filters which follows the simple syntax,
    # and convert them to the appropriate filtering structure
    for key, value in request.args.items():
        # Skip keys that are not filters and are not filter[objects]
        if not key.startswith('filter'):
            continue
        if key in (FILTER_PARAM, FILTER_FUZZY_PARAM):
            continue
        # Get the fieldname on which to filter and the value to match.
        fieldname, value = key[7:-1], value
        # skip filters with no corresponding values
        if not value:
            continue
        # append the formatted filter
        filters.append(format_filter(pri_model, fieldname, value))
    return filters


class JsonApiParser(webargs.flaskparser.FlaskParser):
    """Special case handling for marshmallow-jsonapi.Schema's."""
    schema_ = None

    # This code comes from <#issuecomment-182620019> as work-around to
    # <https://github.com/marshmallow-code/marshmallow-jsonapi/issues/18>
    def _parse_request(self, schema, req, locations):
        class JsonApiNested(ma.Schema):
            """Wrapper for webargs to marshmallow-jsonapi handling."""
            # webargs.flaskparser.FlaskParser
            # specifically returns missing if there is no json
            # at all instead of returning the field default
            data = ma.fields.Dict(required=True)  # missing={}, default={})

            class Meta:
                strict = True

        if isinstance(schema, majapi.Schema):
            jsonapi = JsonApiNested()
            data = super(JsonApiParser, self)\
                ._parse_request(jsonapi, req, locations)
            if data['data'] == webargs.core.missing:
                data = {}
        else:
            data = super(JsonApiParser, self)\
                ._parse_request(schema, req, locations)
        return data or {}

    def parse(self, *args, **kwargs):
        """Overrides default `parse` to get the schema for later use.

        The `schema` saved  will be later on used in parsing
        for filters, sorts and fields in the request.

        """
        # fetch the argmap and request from the arguments
        argmap, req = (
            kwargs.get('argmap', args[0]),
            kwargs.get('req', None))
        # then get the schema
        self.schema_ = self._get_schema(argmap, req)

        # afterwards, do as usual
        return super(webargs.flaskparser.FlaskParser, self)\
            .parse(*args, **kwargs)

    def parse_querystring(self, req, name, field):
        """Perform further parsing for fields, sorts, and filters."""
        if name == 'fields':
            fields = parse_sparse_fields(request=req)
            get_value_ = partial(webargs.core.get_value, dict(fields=fields))
        elif name == 'filters':
            filters = parse_filters(request=req, schema=self.schema_)
            get_value_ = partial(webargs.core.get_value, dict(filters=filters))
        elif name == 'sorts':
            sorts = parse_sorts(request=req)
            get_value_ = partial(webargs.core.get_value, dict(sorts=sorts))
        else:
            get_value_ = partial(webargs.core.get_value, req.args)
        return get_value_(name=name, field=field)


parser = JsonApiParser()
use_args = parser.use_args
use_kwargs = parser.use_kwargs
