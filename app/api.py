from flask import Blueprint

from app.exceptions import ValidationError
from app.errors import bad_request, not_found
from app.auth.authentication import auth

api = Blueprint('api', __name__)


@api.errorhandler(ValidationError)
def validation_error(e):
    return bad_request(e.args[0])


@api.errorhandler(400)
def bad_request_error(e):
    return bad_request(e.args[0])


@api.errorhandler(404)
def not_found_error(e):
    return not_found('item not found')


@api.before_request
@auth.login_required
def before_request():
    """All routes in this blueprint require authentication."""
    pass


# do imports last to avoid circular dependencies

# flake8: noqa
# noinspection PyUnresolvedReferences
import app.users.api
