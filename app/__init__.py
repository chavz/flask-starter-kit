"""Initialize application package.

Attributes:
  db (SQLAlchemy): represents the database and provides access to all
    the functionality of Flask-SQLAlchemy extension.

"""

from config import config
from flask import Flask
from flask_cors import CORS

# instead of using the default `from flask.ext.sqlalchemy import SQLAlchemy()`
# we use an extended version which is more friendlier to tests
from .extensions.ext_flask_sqlalchemy import TestFriendlySQLAlchemy

db = TestFriendlySQLAlchemy()


def create_app(config_name):
    """Create application using the given configuration.

    Application factory which takes as an argument the name of the
    configuration to use for the application. It then returns the
    created application.

    Args:
      config_name (string): name of the configuration.

    Returns:
      Flask: the created application.

    """
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    CORS(app)

    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api/v1')

    from .auth.token import token as api_1_0_token_blueprint
    app.register_blueprint(api_1_0_token_blueprint, url_prefix='/auth')

    return app
