from flask.ext.httpauth import HTTPBasicAuth
from flask import g

from app.users.models import User
from app.errors import unauthorized

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(token, password):
    g.current_user = User.verify_auth_token(token)
    return g.current_user is not None


@auth.error_handler
def auth_error():
    return unauthorized('Invalid credentials')
