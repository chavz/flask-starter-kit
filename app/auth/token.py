from flask.ext.httpauth import HTTPBasicAuth
from flask import Blueprint, g, jsonify, current_app

from app.users.models import User
from app.errors import unauthorized
from app.core.decorators import no_cache

token = Blueprint('token', __name__)
token_auth = HTTPBasicAuth()


@token_auth.verify_password
def verify_password(username_or_token, password):

    # if no username or token given, invalidate.
    if username_or_token == '':
        return False

    # if no password given, assume its a token and verify.
    if password == '':
        g.current_user = User.verify_auth_token(username_or_token)
        return g.current_user is not None

    # else assume its a regular username-password, and verify.
    user = User.query.filter_by(username=username_or_token).first()
    if not user:
        return False
    g.current_user = user
    return user.verify_password(password)


@token_auth.error_handler
def token_auth_error():
    return unauthorized('Invalid credentials')


@token.route('/request-token', methods=['GET'])
@token_auth.login_required
@no_cache
def request_token():
    expiration = current_app.config['AUTH_TOKEN_EXPIRATION']
    return jsonify({'token': g.current_user.generate_auth_token(
        expiration=expiration), 'expiration': expiration})
