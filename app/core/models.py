from itertools import chain
from sqlalchemy_enum34 import EnumType

from app import db
from app.exceptions import ValidationError

from .enums import PartyType


class Party(db.Model):

    __tablename__ = 'parties'

    # columns
    id_ = db.Column(db.Integer, name='id', autoincrement=True)
    type_ = db.Column(EnumType(PartyType, name='party_type'),
                      name='type', nullable=False)
    name = db.Column(db.String(100), nullable=False)

    # constraints and configurations
    __table_args__ = (
        db.PrimaryKeyConstraint('id'),
    )

    @db.validates('type')
    def validate_type(self, key, value):
        if value not in list(chain(*PartyType.__members__.items())):
            raise ValidationError(
                'For key %r, values must be one of the ff. %r'
                % (key, PartyType.__members__.keys()))
        return value

    def __init__(self, type_, name):
        self.type_ = type_
        self.name = name

    def __repr__(self):
        return ('<Party (id=%r, type=%r, name=%r)>'
                % (self.id_, self.type_, self.name))
