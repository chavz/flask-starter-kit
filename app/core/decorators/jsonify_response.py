# Copyright (c) 2014 Miguel Grinberg
#
# This file is a derivative of `oreilly-flask-apis-video`
# released under the MIT License (MIT).
#
# flask-starter-kit is distributed under the 3-clause BSD license,
# for more information, see LICENSE.

from functools import wraps

from flask import jsonify


def jsonify_response(f):
    """Generate a JSON response from a database model or a Python dict."""
    @wraps(f)
    def wrapped(*args, **kwargs):
        # invoke the wrapped function
        rv = f(*args, **kwargs)

        # the wrapped function can return the dictionary alone,
        # or can also include a status code and/or headers.
        # As well as the dynamic serializer as the fourth possible item.
        # Here we separate all these items
        status_or_headers, headers, serializer = None, None, None
        if isinstance(rv, tuple):
            # pad the tuple to it's supposed length of (4)
            # and fill-up the missing values with None
            sequence = rv + (None,) * (4 - len(rv))
            # then unpack them to their respective containers
            rv, status_or_headers, headers, serializer = sequence
        if isinstance(status_or_headers, (dict, list)):
            headers, status_or_headers = status_or_headers, None

        # if the response was a database model,
        # then convert it to its dictionary representation
        # using the default or passed serializer if present
        if not isinstance(rv, dict):
            rv = rv.to_json() if not serializer else getattr(rv, serializer)()

        # generate the json response
        rv = jsonify(rv)

        # if status code and/or headers were present,
        # include them in the response
        if status_or_headers is not None:
            rv.status_code = status_or_headers
        if headers is not None:
            rv.headers.extend(headers)

        # return the response
        return rv
    return wrapped
