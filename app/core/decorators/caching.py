# Copyright (c) 2014 Miguel Grinberg
#
# This file is a derivative of `oreilly-flask-apis-video`
# released under the MIT License (MIT).
#
# flask-starter-kit is distributed under the 3-clause BSD license,
# for more information, see LICENSE.

"""A collection of decorators to handle HTTP Caching."""
import functools
from flask import make_response


def cache_control(*directives):
    """Insert a Cache-Control header with the given directives."""
    def decorator(f):
        @functools.wraps(f)
        def wrapped(*args, **kwargs):
            # invoke the wrapped function
            rv = f(*args, **kwargs)

            # convert the returned value to a response object
            rv = make_response(rv)

            # insert the Cache-Control header and return response
            rv.headers['Cache-Control'] = ', '.join(directives)
            return rv
        return wrapped
    return decorator


def no_cache(f):
    """Insert a no-cache directive in the response.

    This decorator just invokes the cache-control decorator
    with the specific directives.

    """
    return cache_control('private', 'no-cache', 'no-store', 'max-age=0')(f)
