# Copyright (c) 2014 Miguel Grinberg
#
# This file is a derivative of `oreilly-flask-apis-video`
# released under the MIT License (MIT).
#
# flask-starter-kit is distributed under the 3-clause BSD license,
# for more information, see LICENSE.

"""Collection of decorator functions to handle pagination."""

from functools import wraps
from collections import Iterable

from flask.ext.sqlalchemy import BaseQuery
from flask import request, url_for, jsonify

# noinspection PyPackageRequirements
from config import Config
from app.core.utils import paginator


def query(schema, default_page_size=Config.DEFAULT_PAGE_SIZE):
    """Generate a paginated response (if told) for a resource collection.

    Routes that use this decorator must return a SQLAlchemy query as a
    response.

    The output of this decorator is a Python dictionary with the paginated
    results. The application must ensure that this result is converted to a
    response object, either by chaining another decorator or by using a
    custom response object that accepts dictionaries.
    """
    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            # invoke the wrapped function
            qry = f(*args, **kwargs)

            # make sure the response is a BaseQuery
            if not isinstance(qry, BaseQuery):
                raise TypeError

            # obtain the webargs validated pagination arguments
            # from the URL's qry string and assign them.
            paginate = args[0].get('paginate', True)
            page_number = args[0].get('page_number') or 1
            page_size = args[0].get('page_size') or default_page_size

            # create an empty dictionary to contain the response
            response = dict()

            if paginate:
                # run the qry with Flask-SQLAlchemy's pagination
                p = qry.paginate(page=page_number, per_page=page_size)

                # build the pagination metadata to include in the response
                response['total'] = p.total
                response['pages'] = p.pages

                if p.has_prev:
                    response['prev'] = url_for(
                        request.endpoint, page_number=p.prev_num,
                        page_size=page_size, _external=True, **kwargs)
                else:
                    response['prev'] = None

                if p.has_next:
                    response['next'] = url_for(
                        request.endpoint, page_number=p.next_num,
                        page_size=page_size, _external=True, **kwargs)
                else:
                    response['next'] = None

                response['first'] = url_for(
                    request.endpoint, page_number=1,
                    page_size=page_size, _external=True, **kwargs)

                response['last'] = url_for(
                    request.endpoint, page_size=page_size,
                    page_number=p.pages if p.pages > 0 else 1,
                    _external=True, **kwargs)

                # get the paginated collection,
                # later to be converted into dictionary
                items = p.items
            else:
                # run the qry with SqlAlchemy all() function
                # to get the complete result set
                items = qry.all()
                response['total'] = len(items)

            # serialize the fetch items using the schema passed
            serialized_data = schema().dump(items, many=True).data

            # extend the response object with the serialized data
            response = dict(response, **serialized_data)

            # attach top-level links
            response['links'] = {'self': request.url}

            # return the dictionary as a response
            return jsonify(response)
        return wrapped
    return decorator


def iter(schema, default_page_size=Config.DEFAULT_PAGE_SIZE):
    """Generate a paginated response (if told) for a resource collection.

    Routes that use this decorator must return an Iterable as a
    response.

    The output of this decorator is a Python dictionary with the paginated
    results. The application must ensure that this result is converted to a
    response object, either by chaining another decorator or by using a
    custom response object that accepts dictionaries.
    """
    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            # invoke the wrapped function
            iterable = f(*args, **kwargs)

            # make sure the response is an iterable
            if not isinstance(iterable, Iterable):
                raise TypeError

            # obtain the webargs validated pagination arguments
            # from the URL's query string and assign them.
            paginate = args[0].get('paginate', True)
            page_number = args[0].get('page_number') or 1
            page_size = args[0].get('page_size') or default_page_size

            # create an empty dictionary to contain the response
            response = dict()

            # include the total length in the response
            response['total'] = len(iterable)

            if paginate:
                # paginate the iterable
                lis = list(paginator(iterable, page_size))

                # build the pagination metadata to include in the response
                response['pages'] = len(lis)

                if (page_number - 1) > 0:
                    response['prev'] = url_for(
                        request.endpoint, page_number=(page_number - 1),
                        page_size=page_number, _external=True, **kwargs)
                else:
                    response['prev'] = None

                if (page_number + 1) <= len(lis):
                    response['next'] = url_for(
                        request.endpoint, page_number=(page_number + 1),
                        page_size=page_number, _external=True, **kwargs)
                else:
                    response['next'] = None

                response['first'] = url_for(
                    request.endpoint, page_number=1,
                    page_size=page_number, _external=True, **kwargs)

                response['last'] = url_for(
                    request.endpoint, page_size=page_number,
                    page_number=len(lis) if len(lis) > 0 else 1,
                    _external=True, **kwargs)

                if page_number > len(lis):
                    items = lis
                else:
                    # get the paginated collection,
                    # later to be converted into dictionary
                    items = lis[page_number - 1]
            else:
                # get the full data in iterable
                items = iterable

            # serialize the fetch items using the schema passed
            serialized_data = schema().dump(items, many=True).data

            # extend the response object with the serialized data
            response = dict(response, **serialized_data)

            # attach top-level links
            response['links'] = {'self': request.url}

            # return the dictionary as a response
            return jsonify(response)
        return wrapped
    return decorator
