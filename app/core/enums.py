import enum


class PartyType(enum.Enum):
    person = 'person'
    organization = 'organization'
