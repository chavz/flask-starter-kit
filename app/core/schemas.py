import marshmallow as ma

from app.extensions.ext_marshmallow import TupleField


class DefaultUrlSchema(ma.Schema):
    paginate = ma.fields.Bool(load_from='paginate')
    page_number = ma.fields.Int(load_from='page[number]')
    page_size = ma.fields.Int(load_from='page[size]')

    fields = ma.fields.Dict()
    sorts = ma.fields.List(TupleField())
    filters = ma.fields.List(ma.fields.Dict())

    class Meta:
        strict = True
