from marshmallow import validate

no_blanks = validate.Length(min=1, error='Field cannot be blank')
