# searchable.py - exposes the `search` package as a decorator
#
# This file is part of flask-starter-kit .
#
# flask-starter-kit is distributed under the 3-clause BSD license,
# for more information, see LICENSE.
"""Decorator for exposing the search package."""

from functools import wraps

from flask_sqlalchemy import BaseQuery

from . import search
from .filters import FilterParsingError
from ..helpers import get_related_model

from app import db
from app.exceptions import ValidationError


def query(model, session=db.session, resource_id=None, relation_name=None):
    """Generate a filtered, and sorted SQLAlchemy query.

    Routes that use this decorator must return a SQLAlchemy query as a
    response.

    `model` is the SQLAlchemy model on which to create a query.

    `session` is the SQLAlchemy session in which to create the query.

    `resource_id` is the key name to which the resource id can be found.

    `relation_name` is the attribute name to be used to get the
    related model, this is typically a `Relationship` or
    an `AssociatioinProxy` class field.

    """
    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            # invoke the wrapped function
            qry = f(*args, **kwargs)

            # make sure the response is a BaseQuery
            if not isinstance(qry, BaseQuery):
                raise TypeError

            # collect the filtering and sorting fields parsed by webargs
            filters, sorts = (
                args[0].get('filters', None),
                args[0].get('sorts', None)
            )

            # Determine whether this is a request of the form
            # `GET /comments` or `GET /articles/1/comments`.
            if (kwargs.get(resource_id, None) is not None and
                    relation_name is not None):
                primary_model = get_related_model(model, relation_name)
            else:
                primary_model = model
            try:
                qry = search(
                    session=session, model=primary_model,
                    filters=filters, sort=sorts, _initial_query=qry)
            except FilterParsingError:
                raise ValidationError('Invalid Fieldname')
            # return the filtered and sorted query
            return qry
        return wrapped
    return decorator
