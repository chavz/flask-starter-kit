from flask import g

from app import db
from app.api import api
from app.extensions.ext_webargs import use_args
from app.core.decorators import paginate, jsonify_response
from app.search import searchable

from .models import Role, Privilege
from .schemas import (
    RoleSchema, PrivilegeSchema,
    RoleUrlSchema, PrivilegeUrlSchema,
    privilege_map, related_resource)


# ############################ #
# ###### Privileges API ###### #
# ############################ #


@api.route('/privileges', methods=['GET'])
@use_args(PrivilegeUrlSchema, locations=('querystring',))
@paginate.query(PrivilegeSchema, 10)
@searchable.query(Privilege)
def get_privileges(args):
    """Get a collection of privileges."""
    return Privilege.query


@api.route('/privilege/<int:priv_id>', methods=['GET'])
@jsonify_response
def get_privilege(priv_id):
    """Get information about a specific privilege."""
    return Privilege.query.get_or_404(priv_id)


@api.route('/privileges', methods=['POST'])
@use_args(PrivilegeSchema(), locations=('json',))
@jsonify_response
def new_privilege(args):
    """Add new privilege resource."""
    privilege = Privilege.add_from_json(args)
    db.session.add(privilege)
    db.session.commit()

    return {}, 201, {'Location': privilege.get_url()}


@api.route('/privilege/<int:priv_id>', methods=['PUT'])
@use_args(PrivilegeSchema(partial=True), locations=('json',))
@jsonify_response
def update_privilege(args, priv_id):
    """Update information on a specific privilege."""
    privilege = Privilege.query.get_or_404(priv_id)
    privilege.update_from_json(args)
    db.session.add(privilege)
    db.session.commit()
    return {}


@api.route('/user/privilege-map', methods=['POST'])
@use_args(privilege_map, locations=('json',))
@jsonify_response
def privilege_map(args):

    priv_map = g.current_user.privilege_map_from_json(args)

    return {'privilegeMap': priv_map}


@api.route('/user/privileges-all', methods=['GET'])
@jsonify_response
def privileges_all():
    priv_all = g.current_user.privileges

    return {
        'uid': g.current_user.party_id,
        'privileges': [privilege.name for privilege in priv_all]
    }


# ############################ #
# ####### Roles API ########## #
# ############################ #


@api.route('/roles', methods=['GET'])
@use_args(RoleUrlSchema, locations=('query',))
@paginate.query(RoleSchema, 10)
@searchable.query(Role)
def get_roles(args):
    return Role.query


@api.route('/roles/<int:role_id>', methods=['GET'])
@jsonify_response
def get_role(role_id):
    return Role.query.get_or_404(role_id)


@api.route('/roles', methods=['POST'])
@use_args(RoleSchema(), locations=('json',))
@jsonify_response
def new_role(args):
    role = Role.add_from_json(args)
    db.session.add(role)
    db.session.commit()

    return {}, 201, {'Location': role.get_url()}


@api.route('/roles/<int:role_id>', methods=['PUT'])
@use_args(RoleSchema(partial=True), locations=('json',))
@jsonify_response
def update_role(args, role_id):
    role = Role.query.get_or_404(role_id)
    role.update_from_json(args)
    db.session.add(role)
    db.session.commit()
    return {}


@api.route('/roles/<int:role_id>/privileges', methods=['GET'])
@use_args(PrivilegeUrlSchema, locations=('query',))
@paginate.query(PrivilegeSchema, 10)
@searchable.query(Role, resource_id='role_id', relation_name='privileges')
def get_role_privileges(args, role_id):
    Role.query.get_or_404(role_id)
    return Privilege.query\
        .join(*Role.privileges.attr)\
        .filter(Role.id == role_id)


@api.route('/roles/<int:role_id>/privileges', methods=['POST'])
@use_args(related_resource, locations=('json',))
@jsonify_response
def assign_privileges_to_role(args, role_id):
    role = Role.query.get_or_404(role_id)
    role.add_privileges(args['privileges'])
    db.session.add(role)
    db.session.commit()
    return {}


@api.route('/roles/<int:role_id>/privileges', methods=['DELETE'])
@use_args(related_resource, locations=('json',))
@jsonify_response
def remove_privileges_in_role(args, role_id):
    role = Role.query.get_or_404(role_id)
    role.remove_privileges(args['privileges'])
    db.session.add(role)
    db.session.commit()
    return {}
