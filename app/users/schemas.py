from marshmallow_jsonapi import Schema, fields
from marshmallow_jsonapi.flask import Relationship

from app.core.validations import no_blanks
from app.core.schemas import DefaultUrlSchema


related_resource = {
    'privileges': fields.List(fields.Str(), required=True,)
}

privilege_map = {
    'privilegeList': fields.List(fields.Str(), required=True)
}


class RoleUrlSchema(DefaultUrlSchema):
    class Meta:
        strict = True
        model_ = 'Role'


class PrivilegeUrlSchema(DefaultUrlSchema):
    class Meta:
        strict = True
        model_ = 'Privilege'


class RoleSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True, validate=no_blanks)

    privileges = Relationship(
        related_view='api.get_role_privileges',
        related_view_kwargs={'role_id': '<id>'},
        many=True, include_resource_linkage=True,
        type_='privileges'
    )

    class Meta:
        strict = True
        type_ = 'roles'
        model_ = 'Role'
        self_url = '/roles/{id}'
        self_url_kwargs = {'id': '<id>'}
        self_url_many = '/roles'


class PrivilegeSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True, validate=no_blanks)

    class Meta:
        strict = True
        type_ = 'privileges'
        model_ = 'Privilege'
        self_url = '/privileges/{id}'
        self_url_kwargs = {'id': '<id>'}
        self_url_many = '/privileges'
