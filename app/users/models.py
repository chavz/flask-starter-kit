from sqlalchemy.ext.associationproxy import association_proxy
from flask.ext.bcrypt import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from flask import current_app, url_for

from app import db
from app.core.models import Party
from app.core.utils import flatten
from app.exceptions import ValidationError

from .schemas import PrivilegeSchema, RoleSchema


class Privilege(db.Model):

    __tablename__ = 'privileges'
    __pri_search_attr__ = 'name'

    # columns
    id = db.Column(db.Integer, autoincrement=True)
    name = db.Column(db.String(64), nullable=False)

    # constraints and configurations
    __table_args__ = (
        db.UniqueConstraint('name', name='uq_name'),
        db.PrimaryKeyConstraint('id')
    )

    def __init__(self, name=None):
        self.name = name

    def get_url(self):
        return url_for('api.get_privilege', priv_id=self.id, _external=True)

    @classmethod
    def add_from_json(cls, json):
        if not json:
            raise ValidationError('No data submitted.')

        try:
            name = json['name']
        except KeyError as e:
            raise ValidationError('Invalid privilege: missing %s' % e.args[0])

        return cls(name)

    def update_from_json(self, json):
        if not json:
            raise ValidationError('No data submitted.')
        if 'name' in json:
            self.name = json['name']

        return self

    def to_json(self, expanded=False):
        return PrivilegeSchema().dump(self).data

    def __repr__(self):
        return ('<Privilege (id=%r, name=%r)>'
                % (self.id, self.name))


class Role(db.Model):

    __tablename__ = 'roles'
    __pri_search_attr__ = 'name'

    # columns
    id = db.Column(db.Integer, autoincrement=True)
    name = db.Column(db.String(45), nullable=False)

    # constraints and configurations
    __table_args__ = (
        db.UniqueConstraint('name', name='uq_name'),
        db.PrimaryKeyConstraint('id')
    )

    # associations
    privileges = association_proxy('roles_privileges', 'privilege')

    def __init__(self, name=None):
        self.name = name

    def get_url(self):
        return url_for('api.get_role', role_id=self.id, _external=True)

    @classmethod
    def add_from_json(cls, json):
        if not json:
            raise ValidationError('No data submitted.')

        try:
            name = json['name']
        except KeyError as e:
            raise ValidationError('Invalid role: missing %s' % e.args[0])

        return cls(name)

    def update_from_json(self, json):
        if not json:
            raise ValidationError('No data submitted.')

        if 'name' in json:
            self.name = json['name']

        return self

    def to_json(self, expanded=False):
        return RoleSchema().dump(self).data

    def add_privileges(self, *privileges):
        for privilege in list(flatten(privileges)):
            existing_privilege = Privilege.query \
                .filter_by(name=privilege) \
                .first()
            if not existing_privilege:
                raise ValidationError('Invalid privilege: does not exist.')
            if existing_privilege not in self.privileges:
                self.privileges.append(existing_privilege)

    def remove_privileges(self, *privileges):
        for privilege in list(flatten(privileges)):
            existing_privilege = Privilege.query \
                .filter_by(name=privilege) \
                .first()
            if existing_privilege and existing_privilege in self.privileges:
                self.privileges.remove(existing_privilege)

    def __repr__(self):
        return ('<Role (id=%r, name=%r)>'
                % (self.id, self.name))


class RolePrivilege(db.Model):

    __tablename__ = 'role_privileges'

    # columns
    role_id = db.Column(db.Integer)
    privilege_id = db.Column(db.Integer)
    # constraints and configurations
    __table_args__ = (
        db.ForeignKeyConstraint(
            ['role_id'],
            ['roles.id'],
            name='ROLE_PRIV_fk_role_id'
        ),
        db.ForeignKeyConstraint(
            ['privilege_id'],
            ['privileges.id'],
            name='ROLE_PRIV_fk_privilege_id'
        ),
        db.PrimaryKeyConstraint('role_id', 'privilege_id'),
    )

    # relationships
    privilege = db.relationship('Privilege')
    role = db.relationship(
        'Role', backref=db.backref(
            'roles_privileges', cascade='all, delete-orphan'))

    def __init__(self, privilege=None, role=None):
        self.privilege = privilege
        self.role = role

    def __repr__(self):
        return ('<RolePrivilege (role_id=%r, privilege_id=%r)>'
                % (self.role_id, self.privilege_id))


class User(db.Model):

    """Contain information about a user of the system.

    Attributes:
      party_id (int):
      username (str):
      password_hash (str):

    """

    __tablename__ = 'users'

    # columns
    party_id = db.Column(db.Integer)
    username = db.Column(db.String(25), nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)

    # constraints and configurations
    __table_args__ = (
        db.ForeignKeyConstraint(
            ['party_id'],
            ['parties.id'],
            name='users_fk_party_id'
        ),
        db.UniqueConstraint('username', name='uq_username'),
        db.PrimaryKeyConstraint('party_id'),
    )

    # relationships
    owner = db.relationship(
        Party, backref=db.backref('user_account', uselist=False))

    # associations
    user_privileges = association_proxy('users_privileges', 'privilege')
    user_roles = association_proxy('users_roles', 'role')

    def __init__(self, username, password):
        """Create a new User instance.

        Args:
          username (str):
          password (str):

        """
        self.username = username
        if password is not None:
            self.password = password

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """Verify the given password string checks with the user's saved password.

        Args:
          password (str): String to verify

        """
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expiration):
        s = Serializer(current_app.config['SECRET_KEY'],
                       expires_in=expiration)
        return s.dumps({'user': self.party_id}).decode('ascii')

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None
        return User.query.get(data['user'])

    @property
    def privileges(self):
        return (
            Privilege.query
            .outerjoin(
                UserPrivilege,
                Privilege.id == UserPrivilege.privilege_id)
            .outerjoin(
                RolePrivilege,
                Privilege.id == RolePrivilege.privilege_id)
            .outerjoin(
                UserRole,
                RolePrivilege.role_id == UserRole.role_id)
            .filter(
                db.or_(UserPrivilege.user_id == self.party_id,
                       UserRole.user_id == self.party_id))
            .all()
        )

    def privilege_map(self, *privileges):
        """Map a list of privileges against user's all owned privileges.

        This takes the given privileges list and run it against all owned
        privileges of the current user instance, may it be from a role
        designation or a personal privilege designation, and return a
        dictionary containing the results.

        Args:
          *privileges: Variable length of privilege list to map

        Returns:
          dict: A collection of key-value pairs with the given privileges
            as the keys and a bool True or False as its value.

        """
        priv_map = (
            db.session
            .query(
                Privilege.name.label('privilege'),
                db.func.max(
                    db.or_(UserPrivilege.privilege_id.isnot(None),
                           RolePrivilege.privilege_id.isnot(None)))
                .label('value'))
            .select_from(User)
            .join(Privilege, db.literal_column('1'))  # mimic a cross join
            .outerjoin(
                UserPrivilege,
                db.and_(UserPrivilege.user_id == User.party_id,
                        Privilege.id == UserPrivilege.privilege_id))
            .outerjoin(UserRole, UserRole.user_id == User.party_id)
            .outerjoin(
                RolePrivilege,
                db.and_(RolePrivilege.role_id == UserRole.role_id,
                        Privilege.id == RolePrivilege.privilege_id))
            .filter(User.party_id == self.party_id)
            .filter(Privilege.name.in_(list(flatten(privileges))))
            .group_by(Privilege.name)
            .all()
        )

        return dict(priv_map)

    def privilege_map_from_json(self, json):
        if json is None:
            raise ValidationError('No data was submitted.')

        try:
            priv_list = json['privilegeList']
        except KeyError as e:
            raise ValidationError('Invalid request: missing %s' % e.args[0])

        return self.privilege_map(priv_list)

    def add_user_privileges(self, *privileges):
        for privilege in list(flatten(privileges)):
            existing_privilege = (
                Privilege.query
                .filter_by(name=privilege)
                .first()
            )
            if not existing_privilege:
                raise ValidationError('Invalid privilege: does not exist.')
            if existing_privilege not in self.user_privileges:
                self.user_privileges.append(existing_privilege)

    def remove_user_privileges(self, *privileges):
        for privilege in list(flatten(privileges)):
            existing_privilege = (
                Privilege.query
                .filter_by(name=privilege)
                .first()
            )
            if (existing_privilege and
                    existing_privilege in self.user_privileges):
                self.user_privileges.remove(existing_privilege)

    def add_user_roles(self, *roles):
        for role in list(flatten(roles)):
            existing_role = (
                Role.query
                .filter_by(name=role)
                .first()
            )
            if not existing_role:
                raise ValidationError('Invalid role: does not exist.')
            if existing_role not in self.user_roles:
                self.user_roles.append(existing_role)

    def remove_user_roles(self, *roles):
        for role in list(flatten(roles)):
            existing_role = (
                Role.query
                .filter_by(name=role)
                .first()
            )
            if (existing_role and
                    existing_role in self.user_roles):
                self.user_roles.remove(existing_role)

    def __repr__(self):
        """Return the string representation of a user."""
        return ('<User (party_id=%r, username=%r)>'
                % (self.party_id, self.username))


class UserRole(db.Model):

    __tablename__ = 'user_roles'

    # columns
    user_id = db.Column(db.Integer, nullable=False)
    role_id = db.Column(db.Integer, nullable=False)

    # constraints and configurations
    __table_args__ = (
        db.ForeignKeyConstraint(
            ['user_id'],
            ['users.party_id'],
            name='USER_ROLE_fk_user_id'
        ),
        db.ForeignKeyConstraint(
            ['role_id'],
            ['roles.id'],
            name='USER_ROLE_fk_role_id'
        ),
        db.PrimaryKeyConstraint('user_id', 'role_id'),
    )

    # relationships
    role = db.relationship('Role')
    user = db.relationship(
        'User', backref=db.backref(
            'users_roles', cascade='all, delete-orphan'))

    def __init__(self, role=None, user=None):
        self.role = role
        self.user = user

    def __repr__(self):
        return '<UserRole (user_id=%r, role_id=%r)>' \
            % (self.user_id, self.role_id)


class UserPrivilege(db.Model):

    __tablename__ = 'user_privileges'

    # columns
    user_id = db.Column(db.Integer, nullable=False)
    privilege_id = db.Column(db.Integer, nullable=False)

    # constraints and configurations
    __table_args__ = (
        db.ForeignKeyConstraint(
            ['user_id'],
            ['users.party_id'],
            name='USER_PRIV_fk_user_id'
        ),
        db.ForeignKeyConstraint(
            ['privilege_id'],
            ['privileges.id'],
            name='USER_PRIV_fk_privilege_id'
        ),
        db.PrimaryKeyConstraint('user_id', 'privilege_id'),
    )

    # relationships
    privilege = db.relationship('Privilege')
    user = db.relationship(
        'User', backref=db.backref(
            'users_privileges', cascade='all, delete-orphan'))

    def __init__(self, privilege=None, user=None):
        self.privilege = privilege
        self.user = user

    def __repr__(self):
        return '<UserPrivilege (user_id=%r, privilege_id=%r)>' \
            % (self.user_id, self.privilege_id)
