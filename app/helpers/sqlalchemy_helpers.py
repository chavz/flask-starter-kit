# sqlalchemy_helpers.py - SQLAlchemy helper functions for flask-starter-kit
#
# Copyright 2011 Lincoln de Sousa <lincoln@comum.org>.
# Copyright 2012, 2013, 2014, 2015, 2016 Jeffrey Finkelstein
#           <jeffrey.finkelstein@gmail.com> and contributors.
# Copyright 2016 Flask-Restless under the 3-clause BSD license.
#
# This file is part of flask-starter-kit.
#
# flask-starter-kit is distributed under the 3-clause BSD license,
# for more information, see LICENSE.
"""SQLAlchemy helper functions for flask-starter-kit.

Many of the functions in this module use the `SQLAlchemy inspection
API`_. As a rule, however, these functions do not catch
:exc:`sqlalchemy.exc.NoInspectionAvailable` exceptions; the
responsibility is on the calling function to ensure that functions that
expect, for example, a SQLAlchemy model actually receive a SQLAlchemy
model.

.. _SQLAlchemy inspection API: https://docs.sqlalchemy.org/en/latest/core/inspection.html

"""

# flake8: noqa

import datetime
import inspect

from dateutil.parser import parse as parse_datetime
from sqlalchemy import Date, Time, DateTime, Interval
from sqlalchemy.ext.associationproxy import AssociationProxy
from sqlalchemy.orm import RelationshipProperty as RelProperty
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import ColumnElement
from sqlalchemy.inspection import inspect as sqlalchemy_inspect

from werkzeug.urls import url_quote_plus

#: Strings which, when received by the server as the value of a date or time
#: field, indicate that the server should use the current time when setting the
#: value of the field.
CURRENT_TIME_MARKERS = ('CURRENT_TIMESTAMP', 'CURRENT_DATE', 'LOCALTIMESTAMP')


def session_query(session, model):
    """Returns a SQLAlchemy query object for the specified `model`.

    If `model` has a ``query`` attribute already, ``model.query`` will be
    returned. If the ``query`` attribute is callable ``model.query()`` will be
    returned instead.

    If `model` has no such attribute, a query based on `session` will be
    created and returned.

    """
    if hasattr(model, 'query'):
        if callable(model.query):
            query = model.query()
        else:
            query = model.query
        if hasattr(query, 'filter'):
            return query
    return session.query(model)


def get_related_model(model, relationname):
    """Gets the class of the model to which `model` is related by the attribute
    whose name is `relationname`.

    For example, if we have the model classes ::

        class Person(Base):
            __tablename__ = 'person'
            id = Column(Integer, primary_key=True)
            articles = relationship('Article')

        class Article(Base):
            __tablename__ = 'article'
            id = Column(Integer, primary_key=True)
            author_id = Column(Integer, ForeignKey('person.id'))
            author = relationship('Person')

    then ::

        >>> get_related_model(Person, 'articles')
        <class 'Article'>
        >>> get_related_model(Article, 'author')
        <class 'Person'>

    This function also "sees through" association proxies and returns
    the model of the proxied remote relation.

    """
    mapper = sqlalchemy_inspect(model)
    attribute = mapper.all_orm_descriptors[relationname]
    # HACK This is required for Python 3.3 only. I'm guessing it lazily
    # loads the attribute or something like that.
    hasattr(model, relationname)
    return get_related_model_from_attribute(attribute)


def get_related_model_from_attribute(attribute):
    """Gets the class of the model related to the given attribute via
    the given name.

    `attribute` may be an
    :class:`~sqlalchemy.orm.attributes.InstrumentedAttribute` or an
    :class:`~sqlalchemy.ext.associationproxy.AssociationProxy`, for
    example ``Article.comments`` or ``Comment.tags``. This function
    "sees through" association proxies to return the model of the
    proxied remote relation.

    """
    if isinstance(attribute, AssociationProxy):
        return attribute.remote_attr.mapper.class_
    return attribute.property.mapper.class_


def primary_key_names(model):
    """Returns a list of all the primary keys for a model.

    The returned list contains the name of each primary key as a string.

    """
    mapper = sqlalchemy_inspect(model)
    return [column.name for column in mapper.primary_key]


def primary_key_value(instance, as_string=False):
    """Returns the value of the primary key field of the specified `instance`
    of a SQLAlchemy model.

    This essentially a convenience function for::

        getattr(instance, primary_key_for(instance))

    If `as_string` is ``True``, try to coerce the return value to a string.

    """
    result = getattr(instance, primary_key_for(instance))
    if not as_string:
        return result
    try:
        return str(result)
    except UnicodeEncodeError:
        return url_quote_plus(result.encode('utf-8'))


def is_like_list(model_or_instance, relationname):
    """Decides whether a relation of a SQLAlchemy model is list-like.

    A relation may be like a list if it behaves like a to-many relation
    (either lazy or eager)

    `model_or_instance` may be either a SQLAlchemy model class or an
    instance of such a class.

    `relationname` is a string naming a relationship of the given
    model or instance.

    """
    # Use Python's built-in inspect module to decide whether the
    # argument is a model or an instance of a model.
    if not inspect.isclass(model_or_instance):
        model = get_model(model_or_instance)
    else:
        model = model_or_instance
    mapper = sqlalchemy_inspect(model)
    relation = mapper.all_orm_descriptors[relationname]
    if isinstance(relation, AssociationProxy):
        relation = relation.local_attr
    return relation.property.uselist


def get_field_type(model, fieldname):
    """Returns the SQLAlchemy type of the field.

    This works for plain columns and association proxies. If `fieldname`
    specifies a hybrid property, this function returns `None`.

    """
    field = getattr(model, fieldname)
    if isinstance(field, ColumnElement):
        return field.type
    if isinstance(field, AssociationProxy):
        field = field.remote_attr
    if hasattr(field, 'property'):
        prop = field.property
        if isinstance(prop, RelProperty):
            return None
        return prop.columns[0].type
    return None


def string_to_datetime(model, fieldname, value):
    """Casts `value` to a :class:`datetime.datetime` or
    :class:`datetime.timedelta` object if the given field of the given
    model is a date-like or interval column.

    If the field name corresponds to a field in the model which is a
    :class:`sqlalchemy.types.Date`, :class:`sqlalchemy.types.DateTime`,
    or :class:`sqlalchemy.Interval`, then the returned value will be the
    :class:`datetime.datetime` or :class:`datetime.timedelta` Python
    object corresponding to `value`. Otherwise, the `value` is returned
    unchanged.

    """
    if value is None:
        return value
    # If this is a date, time or datetime field, parse it and convert it to
    # the appropriate type.
    field_type = get_field_type(model, fieldname)
    if isinstance(field_type, (Date, Time, DateTime)):
        # If the string is empty, no datetime can be inferred from it.
        if value.strip() == '':
            return None
        # If the string is a string indicating that the value of should be the
        # current datetime on the server, get the current datetime that way.
        if value in CURRENT_TIME_MARKERS:
            return getattr(func, value.lower())()
        value_as_datetime = parse_datetime(value)
        # If the attribute on the model needs to be a Date or Time object as
        # opposed to a DateTime object, just get the date component of the
        # datetime.
        if isinstance(field_type, Date):
            return value_as_datetime.date()
        if isinstance(field_type, Time):
            return value_as_datetime.timetz()
        return value_as_datetime
    # If this is an Interval field, convert the integer value to a timedelta.
    if isinstance(field_type, Interval) and isinstance(value, int):
        return datetime.timedelta(seconds=value)
    # In any other case, simply copy the value unchanged.
    return value


def get_model(instance):
    """Returns the model class of which the specified object is an instance."""
    return type(instance)


def is_relationship(model, fieldname):
    """Decides whether a field is a relationship (as opposed to a
    field).

    `model` is a SQLAlchemy model.

    `fieldname` is a string naming a field of the given model. This
    function returns True if and only if the field is a relationship.

    """
    mapper = sqlalchemy_inspect(model)
    if fieldname in mapper.relationships:
        return True
    if isinstance(mapper.all_orm_descriptors[fieldname], AssociationProxy):
        return True
    if fieldname in [
            synonyms.key for synonyms in mapper.synonyms
            if isinstance(synonyms._proxied_property, RelProperty)]:
        return True

    return False


# This code comes from <http://stackoverflow.com/a/6798042/108197>, which is
# licensed under the Creative Commons Attribution-ShareAlike License version
# 3.0 Unported.
#
# That is an answer originally authored by the user
# <http://stackoverflow.com/users/500584/agf> to the question
# <http://stackoverflow.com/q/6760685/108197>.
#
# TODO This code is for simultaneous Python 2 and 3 usage. It can be greatly
# simplified when removing Python 2 support.
class _Singleton(type):
    """A metaclass for a singleton class."""

    #: The known instances of the class instantiating this metaclass.
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """Returns the singleton instance of the specified class."""
        if cls not in cls._instances:
            supercls = super(_Singleton, cls)
            cls._instances[cls] = supercls.__call__(*args, **kwargs)
        return cls._instances[cls]


class Singleton(_Singleton('SingletonMeta', (object,), {})):
    """Base class for a singleton class."""
    pass


class KnowsAPIManagers:
    """An object that allows client code to register :class:`APIManager`
    objects.

    """

    def __init__(self):
        #: A global list of created :class:`APIManager` objects.
        self.created_managers = set()

    def register(self, apimanager):
        """Inform this object about the specified :class:`APIManager` object.

        """
        self.created_managers.add(apimanager)


class PrimaryKeyFinder(KnowsAPIManagers, Singleton):
    """The singleton class that backs the :func:`primary_key_for` function."""

    def __call__(self, instance_or_model, _apimanager=None, **kw):
        if isinstance(instance_or_model, type):
            model = instance_or_model
        else:
            model = instance_or_model.__class__

        if _apimanager is not None:
            managers_to_search = [_apimanager]
        else:
            managers_to_search = self.created_managers
        for manager in managers_to_search:
            if model in manager.created_apis_for:
                primary_key = manager.primary_key_for(model, **kw)
                break
        else:
            message = ('Model "{0}" is not known to {1}; maybe you have not'
                       ' called APIManager.create_api() for this model?')
            if _apimanager is not None:
                manager_string = 'APIManager "{0}"'.format(_apimanager)
            else:
                manager_string = 'any APIManager objects'
            message = message.format(model, manager_string)
            raise ValueError(message)

        # If `APIManager.create_api(model)` was called without providing
        # a value for the `primary_key` keyword argument, then we must
        # compute the primary key name from the model directly.
        if primary_key is None:
            pk_names = primary_key_names(model)
            primary_key = 'id' if 'id' in pk_names else pk_names[0]
        return primary_key


#: Returns the primary key to be used for the given model or model instance,
#: as specified by the ``primary_key`` keyword argument to
#: :meth:`APIManager.create_api` when it was previously invoked on the model.
#:
#: `primary_key` is a string corresponding to the primary key identifier
#: to be used by flask-restless for a model. If no primary key has been set
#: at the flask-restless level (by using the ``primary_key`` keyword argument
#: when calling :meth:`APIManager.create_api_blueprint`, the model's primary
#: key will be returned. If no API has been created for the model, this
#: function raises a `ValueError`.
#:
#: If `_apimanager` is not ``None``, it must be an instance of
#: :class:`APIManager`. Restrict our search for endpoints exposing `model` to
#: only endpoints created by the specified :class:`APIManager` instance.
#:
#: For example, suppose you have a model class ``Person`` and have created the
#: appropriate Flask application and SQLAlchemy session::
#:
#:     >>> from mymodels import Person
#:     >>> manager = APIManager(app, session=session)
#:     >>> manager.create_api(Person, primary_key='name')
#:     >>> primary_key_for(Person)
#:     'name'
#:     >>> my_person = Person(name="Bob")
#:     >>> primary_key_for(my_person)
#:     'name'
#:
#: This is in contrast to the typical default:
#:
#:     >>> manager = APIManager(app, session=session)
#:     >>> manager.create_api(Person)
#:     >>> primary_key_for(Person)
#:     'id'
#:
primary_key_for = PrimaryKeyFinder()
