"""add core module tables

Revision ID: f149c4d147d3
Revises: None
Create Date: 2016-08-14 09:12:43.275447

"""

# revision identifiers, used by Alembic.
revision = 'f149c4d147d3'
down_revision = None

from alembic import op
import sqlalchemy as sa

from alembic import context
from sqlalchemy import sql as el  # NOQA: sql expression language

from sqlalchemy_enum34 import EnumType
from app.core.enums import PartyType


def upgrade():
    schema_upgrades()
    if context.get_x_argument(as_dictionary=True).get('data', None):
        data_upgrades()


def downgrade():
    if context.get_x_argument(as_dictionary=True).get('data', None):
        data_downgrades()
    schema_downgrades()


def schema_upgrades():
    """schema upgrade migrations go here."""
    # parties table.
    op.create_table(
        'parties',
        sa.Column('id', sa.Integer),
        sa.Column('type', EnumType(PartyType), nullable=False),
        sa.Column('name', sa.String(100), nullable=False)
    )
    op.create_primary_key(
        constraint_name='`PRIMARY`',
        table_name='parties',
        columns=['id']
    )
    op.alter_column(
        table_name='parties',
        column_name='id',
        existing_type=sa.Integer,
        autoincrement=True,
        nullable=False
    )


def schema_downgrades():
    """schema downgrade migrations go here."""
    op.drop_table('parties')


def data_upgrades():
    """Add any optional data upgrade migrations here."""
    # Create ad-hoc tables to use for the insert statements
    parties_table = el.table(
        'parties',
        el.column('id', sa.Integer),
        el.column('type', EnumType(PartyType)),
        el.column('name', sa.String())
    )

    # perform the bulk insert in proper order,
    # following foreign key constraints
    op.bulk_insert(
        parties_table,
        [
            {'id': 1, 'type': PartyType.person, 'name': 'John Doe'},
            {'id': 2, 'type': PartyType.person, 'name': 'Jane Doe'},
            {'id': 3, 'type': PartyType.person, 'name': 'Jane Roe'},
            {'id': 4, 'type': PartyType.person, 'name': 'Jonnie Doe'},
            {'id': 5, 'type': PartyType.person, 'name': 'Janie Doe'}
        ]
    )


def data_downgrades():
    """Add any optional data downgrade migrations here."""
    # Create ad-hoc tables to use for the delete statements
    parties_table = el.table(
        'parties',
        el.column('id', sa.Integer),
        el.column('type', EnumType(PartyType)),
        el.column('name', sa.String(100))
    )

    # perform the row deletions
    op.execute(
        parties_table.delete().where(parties_table.c.id.in_([1, 2, 3, 4, 5]))
    )
