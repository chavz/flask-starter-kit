"""add users module tables

Revision ID: c77cb8d558cb
Revises: f149c4d147d3
Create Date: 2016-08-14 09:17:04.797809

"""

# revision identifiers, used by Alembic.
revision = 'c77cb8d558cb'
down_revision = 'f149c4d147d3'

from alembic import op
import sqlalchemy as sa

from alembic import context
from sqlalchemy import sql as el  # NOQA: sql expression language


def upgrade():
    schema_upgrades()
    if context.get_x_argument(as_dictionary=True).get('data', None):
        data_upgrades()


def downgrade():
    if context.get_x_argument(as_dictionary=True).get('data', None):
        data_downgrades()
    schema_downgrades()


def schema_upgrades():
    """schema upgrade migrations go here."""
    # users table.
    op.create_table(
        'users',
        sa.Column('party_id', sa.Integer),
        sa.Column('username', sa.String(45), nullable=False),
        sa.Column('password_hash', sa.String(128), nullable=False)
    )
    op.create_foreign_key(
        constraint_name='users_fk_party_id',
        source_table='users',
        referent_table='parties',
        local_cols=['party_id'],
        remote_cols=['id']
    )
    op.create_primary_key(
        constraint_name='`PRIMARY`',
        table_name='users',
        columns=['party_id']
    )
    op.create_unique_constraint(
        constraint_name='uq_username',
        table_name='users',
        columns=['username']
    )
    op.create_index(
        index_name='idx_username',
        table_name='users',
        columns=['username']
    )


def schema_downgrades():
    """schema downgrade migrations go here."""
    op.drop_table('users')


def data_upgrades():
    """Add any optional data upgrade migrations here."""
    # Create ad-hoc tables to use for the insert statements
    users_table = el.table(
        'users',
        el.column('party_id', sa.Integer),
        el.column('username', sa.String()),
        el.column('password_hash', sa.String())
    )

    # perform the bulk insert in proper order,
    # following foreign key constraints
    op.bulk_insert(
        users_table,
        [  # password_hash corresponds to string 'password'
            {'party_id': 1, 'username': 'johndoe',
             'password_hash': ('$2a$12$dM3jbPZccUusf2Ar91wr'
                               'quI8iXoC94VAPRCPkElbhU5Mtg5HGC/.u')},
            {'party_id': 2, 'username': 'janedoe',
             'password_hash': ('$2a$12$rmSldsE85kUUyVYda09R'
                               'KOtGrLpPXDSFAJyjdugVL2ge/tKtZdImi')},
            {'party_id': 3, 'username': 'janeroe',
             'password_hash': ('$2a$12$KnnR35wvUVSx1gfO/jsl'
                               'l.7JGnL9A1NgoN0BQtkMfAGBHdlkAao2e')},
            {'party_id': 4, 'username': 'jonniedoe',
             'password_hash': ('$2a$12$B8YO8kUKbY6IPmWDVV0T'
                               'XejvIV.x9DiZgbljoipGYHHbCgzWUm9LG')},
            {'party_id': 5, 'username': 'janiedoe',
             'password_hash': ('$2a$12$h1fQxxma0mT6KRMSdBtd'
                               '7ekSy2TSqR1wWm/GViB/ER6yXGYpVTuZm')}
        ]
    )


def data_downgrades():
    """Add any optional data downgrade migrations here."""
    # Create ad-hoc tables to use for the delete statements
    users_table = el.table(
        'users',
        el.column('party_id', sa.Integer),
        el.column('username', sa.String(45)),
        el.column('password_hash', sa.String(128))
    )

    # perform the row deletions
    op.execute(
        users_table.delete().where(users_table.c.party_id.in_([1, 2, 3, 4, 5]))
    )
