"""add roles and privileges table

Revision ID: 7dcd2dadbb25
Revises: c77cb8d558cb
Create Date: 2016-08-23 19:40:38.361433

"""

# revision identifiers, used by Alembic.
revision = '7dcd2dadbb25'
down_revision = 'c77cb8d558cb'

from alembic import op
import sqlalchemy as sa

from alembic import context
from sqlalchemy import sql as el  # NOQA: sql expression language


def upgrade():
    schema_upgrades()
    if context.get_x_argument(as_dictionary=True).get('data', None):
        data_upgrades()


def downgrade():
    if context.get_x_argument(as_dictionary=True).get('data', None):
        data_downgrades()
    schema_downgrades()


def schema_upgrades():
    """schema upgrade migrations go here."""
    # `privileges` table.
    op.create_table(
        'privileges',
        sa.Column('id', sa.Integer),
        sa.Column('name', sa.String(45), nullable=False)
    )
    op.create_primary_key(
        name='`PRIMARY`',
        table_name='privileges',
        cols=['id']
    )
    op.alter_column(
        table_name='privileges',
        column_name='id',
        existing_type=sa.Integer,
        autoincrement=True,
        nullable=False
    )
    op.create_unique_constraint(
        name='uq_name',
        source='privileges',
        local_cols=['name']
    )
    op.create_index(
        name='idx_name',
        table_name='privileges',
        columns=['name']
    )

    # `roles` table.
    op.create_table(
        'roles',
        sa.Column('id', sa.Integer),
        sa.Column('name', sa.String(45), nullable=False)
    )
    op.create_primary_key(
        name='`PRIMARY`',
        table_name='roles',
        cols=['id']
    )
    op.alter_column(
        table_name='roles',
        column_name='id',
        existing_type=sa.Integer,
        autoincrement=True,
        nullable=False
    )
    op.create_unique_constraint(
        name='uq_name',
        source='roles',
        local_cols=['name']
    )
    op.create_index(
        name='idx_name',
        table_name='roles',
        columns=['name']
    )

    # `role_privileges` table.
    op.create_table(
        'role_privileges',
        sa.Column('role_id', sa.Integer),
        sa.Column('privilege_id', sa.Integer)
    )
    op.create_foreign_key(
        name='ROLE_PRIV_fk_role_id',
        source='role_privileges',
        referent='roles',
        local_cols=['role_id'],
        remote_cols=['id']
    )
    op.create_foreign_key(
        name='ROLE_PRIV_fk_privilege_id',
        source='role_privileges',
        referent='privileges',
        local_cols=['privilege_id'],
        remote_cols=['id']
    )
    op.create_primary_key(
        name='`PRIMARY`',
        table_name='role_privileges',
        cols=['role_id', 'privilege_id']
    )

    # `user_privileges` table.
    op.create_table(
        'user_privileges',
        sa.Column('user_id', sa.Integer),
        sa.Column('privilege_id', sa.Integer)
    )
    op.create_foreign_key(
        name='USER_PRIV_fk_party_id',
        source='user_privileges',
        referent='users',
        local_cols=['user_id'],
        remote_cols=['party_id']
    )
    op.create_foreign_key(
        name='USER_PRIV_fk_privilege_id',
        source='user_privileges',
        referent='privileges',
        local_cols=['privilege_id'],
        remote_cols=['id']
    )
    op.create_primary_key(
        name='`PRIMARY`',
        table_name='user_privileges',
        cols=['user_id', 'privilege_id']
    )

    # `user_roles` table.
    op.create_table(
        'user_roles',
        sa.Column('user_id', sa.Integer),
        sa.Column('role_id', sa.Integer)
    )
    op.create_foreign_key(
        name='USER_ROLE_fk_party_id',
        source='user_roles',
        referent='users',
        local_cols=['user_id'],
        remote_cols=['party_id']
    )
    op.create_foreign_key(
        name='USER_ROLE_fk_role_id',
        source='user_roles',
        referent='roles',
        local_cols=['role_id'],
        remote_cols=['id']
    )
    op.create_primary_key(
        name='`PRIMARY`',
        table_name='user_roles',
        cols=['user_id', 'role_id']
    )


def schema_downgrades():
    """schema downgrade migrations go here."""
    op.drop_table('user_roles')
    op.drop_table('user_privileges')
    op.drop_table('role_privileges')
    op.drop_table('roles')
    op.drop_table('privileges')


def data_upgrades():
    """Add any optional data upgrade migrations here."""
    # Create ad-hoc tables to use for the insert statements
    tbl_privileges = el.table(
        'privileges',
        el.column('id', sa.Integer),
        el.column('name', sa.String(45))
    )
    tbl_roles = el.table(
        'roles',
        el.column('id', sa.Integer),
        el.column('name', sa.String(45))
    )
    tbl_role_privileges = el.table(
        'role_privileges',
        el.column('role_id', sa.Integer),
        el.column('privilege_id', sa.Integer)
    )
    tbl_user_privileges = el.table(
        'user_privileges',
        el.column('user_id', sa.Integer),
        el.column('privilege_id', sa.Integer)
    )
    tbl_user_roles = el.table(
        'user_roles',
        el.column('user_id', sa.Integer),
        el.column('role_id', sa.Integer)
    )

    # perform the bulk insert in proper order,
    # following foreign key constraints
    op.bulk_insert(
        tbl_privileges,
        [
            {'id': 1, 'name': 'is_a'},
            {'id': 2, 'name': 'is_b'},
            {'id': 3, 'name': 'is_c'},
            {'id': 4, 'name': 'is_d'},
            {'id': 5, 'name': 'is_e'},
            {'id': 6, 'name': 'is_f'},
            {'id': 7, 'name': 'is_g'},
            {'id': 8, 'name': 'is_h'},
            {'id': 9, 'name': 'is_i'},
            {'id': 10, 'name': 'is_j'},
            {'id': 11, 'name': 'is_k'},
            {'id': 12, 'name': 'is_l'},
            {'id': 13, 'name': 'is_m'},
            {'id': 14, 'name': 'is_n'},
            {'id': 15, 'name': 'is_o'},
            {'id': 16, 'name': 'is_p'},
            {'id': 17, 'name': 'is_q'},
            {'id': 18, 'name': 'is_r'},
            {'id': 19, 'name': 'is_s'},
            {'id': 20, 'name': 'is_t'},
            {'id': 21, 'name': 'is_u'},
            {'id': 22, 'name': 'is_v'},
            {'id': 23, 'name': 'is_w'},
            {'id': 24, 'name': 'is_x'},
            {'id': 25, 'name': 'is_y'},
            {'id': 26, 'name': 'is_z'},
            {'id': 27, 'name': 'is_allow_manager_approval'}
        ]
    )
    op.bulk_insert(
        tbl_roles,
        [
            {'id': 1, 'name': 'Role1'},
            {'id': 2, 'name': 'Role2'},
            {'id': 3, 'name': 'Role3'},
            {'id': 4, 'name': 'Role4'},
            {'id': 5, 'name': 'Role5'},
            {'id': 6, 'name': 'Role6'},
            {'id': 7, 'name': 'Role7'},
            {'id': 8, 'name': 'Role8'},
            {'id': 9, 'name': 'Role9'}
        ]
    )
    op.bulk_insert(
        tbl_role_privileges,
        [
            {'role_id': 1, 'privilege_id': 11},
            {'role_id': 1, 'privilege_id': 12},
            {'role_id': 2, 'privilege_id': 13},
            {'role_id': 2, 'privilege_id': 14},
            {'role_id': 3, 'privilege_id': 15},
            {'role_id': 3, 'privilege_id': 16},
            {'role_id': 4, 'privilege_id': 17},
            {'role_id': 4, 'privilege_id': 18},
            {'role_id': 5, 'privilege_id': 19},
            {'role_id': 5, 'privilege_id': 20},
            {'role_id': 6, 'privilege_id': 21},
            {'role_id': 6, 'privilege_id': 22},
            {'role_id': 7, 'privilege_id': 23},
            {'role_id': 7, 'privilege_id': 24},
            {'role_id': 8, 'privilege_id': 25},
            {'role_id': 8, 'privilege_id': 26}
        ]
    )
    op.bulk_insert(
        tbl_user_roles,
        [
            {'user_id': 1, 'role_id': 1}, {'user_id': 2, 'role_id': 2},
            {'user_id': 3, 'role_id': 3}, {'user_id': 4, 'role_id': 4},
            {'user_id': 5, 'role_id': 5}
        ]
    )
    op.bulk_insert(
        tbl_user_privileges,
        [
            {'user_id': 1, 'privilege_id': 1},
            {'user_id': 2, 'privilege_id': 2},
            {'user_id': 2, 'privilege_id': 27},
            {'user_id': 3, 'privilege_id': 3},
            {'user_id': 4, 'privilege_id': 4},
            {'user_id': 5, 'privilege_id': 5}
        ]
    )


def data_downgrades():
    """Add any optional data downgrade migrations here."""
    # Create ad-hoc tables to use for the insert statements
    tbl_privileges = el.table(
        'privileges',
        el.column('id', sa.Integer),
        el.column('name', sa.String())
    )
    tbl_roles = el.table(
        'roles',
        el.column('id', sa.Integer),
        el.column('name', sa.String())
    )
    tbl_role_privileges = el.table(
        'role_privileges',
        el.column('role_id', sa.Integer),
        el.column('privilege_id', sa.Integer)
    )
    tbl_user_privileges = el.table(
        'user_privileges',
        el.column('user_id', sa.Integer),
        el.column('privilege_id', sa.Integer)
    )
    tbl_user_roles = el.table(
        'user_roles',
        el.column('user_id', sa.Integer),
        el.column('role_id', sa.Integer)
    )

    # perform the row deletions
    op.execute(
        tbl_user_roles
        .delete()
        .where(el.and_(
            tbl_user_roles.c.user_id > 0,
            tbl_user_roles.c.user_id < 6
        ))
    )
    op.execute(
        tbl_user_privileges
        .delete()
        .where(el.and_(
            tbl_user_privileges.c.user_id > 0,
            tbl_user_privileges.c.user_id < 6
        ))
    )
    op.execute(
        tbl_role_privileges
        .delete()
        .where(el.and_(
            tbl_role_privileges.c.role_id > 0,
            tbl_role_privileges.c.role_id < 9
        ))
    )
    op.execute(
        tbl_roles
        .delete()
        .where(el.and_(
            tbl_roles.c.id > 0,
            tbl_roles.c.id < 9
        ))
    )

    op.execute(
        tbl_privileges
        .delete()
        .where(el.and_(
            tbl_privileges.c.id > 0,
            tbl_privileges.c.id < 27
        ))
    )
