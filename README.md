# Flask Starter Kit

My very own REST enabled flask starter kit which is highly influenced by Miguel Grinberg's book *Flask Web Development* and follows the *jsonapi.org* specification for structuring the REST API.

## Technologies used:
 + `Flask-SQLAlchemy` for the ORM/ODM
 + `alembic` & `Flask-Migrate` for handling database migrations.
 + `webargs` for parsing HTTP requests
 + `marshmallow-jsonapi` for serializing responses in accordance to *jsonapi.org* specifications.
 + `SQLAlchemy-Utils` & `pydash` as utility libraries
 + `pytest` as the default test runner.
 + `factoryboy` for generating objects for testing on the fly.

## File Structure

The code are grouped in a **"feature-per-package"** *(e.g. auth, users, roles, etc.)* structure in contrast to a **"type-per-package"** *(e.g. models, views, api's, etc.)* structure.

Each package in turn contains their own `models`, `views`, `api`, etc. Which gets collected outside their respective packages as deemed appropriate to be included and group into `blueprints` (e.g `app.api`)

```
.
├── app
│   ├── api.py # blueprint w/c consolidates the api modules from different packages
│   ├── auth # contains code managing authenticaion/authorization
│   ├── core
│   │   └── #  contains common code blocks being used app wide
│   ├── errors.py
│   ├── exceptions.py
│   ├── extensions
│   │   ├─- ext_flask_sqlalchemy.py
│   │   └── ... # any other 3rd-party libraries that needs to be extended
│   ├── users # contains all the code for managing users & their roles & privileges
│   │   ├── models.py
│   │   └── api.py
│   └── ... any other additional features group into separate packages
├── config.py  # class-based configurations
├── Makefile 
├── manage.py
├── migrations
├── requirements
│   ├── common.txt
│   ├── development.txt
│   ├── production.txt
│   └── testing.txt
├── static
├── templates
├── tests
│   ├── conftest.py
│   ├── factories
│   ├── mock_client.py
│   └── ... # tests for the different modules
├── tox.ini
└── venv
```

## Advance Sorting & Filtering

For filtering and sorting, the application follows the conventions used 
by `Flask-Restless` which closely adheres to the `jsonapi.org` specifications.

## TODO
 - [ ] Documentation
 - [ ] Appropriate licenses
 - [ ] UI
