import pytest

import os

# look for the environment configuration file,
# if it exists, import them as environment variables.
# TAKE NOTE: this must be done before we import config module
# see https://github.com/miguelgrinberg/flasky/issues/4#issuecomment-37540289
if os.path.exists('.env'):
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from app import create_app
from app import db as _db

from tests.mock_client import TestClient
# noinspection PyUnresolvedReferences
from tests.factories import *  # flake8: noqa


@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    app = create_app('testing')

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope='session')
def db(app, request):
    """Session-wide test database."""
    def teardown():
        # drop the schema/tables
        _db.drop_all()

    _db.app = app

    # create the schema/tables
    _db.create_all()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope='function')
def session(db, request):
    """Creates a new database session for a test."""
    # connect to the database
    connection = db.engine.connect()
    # begin a non-ORM transaction
    transaction = connection.begin()

    # bind an individual session to the connection
    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    # overload the default session with the session above
    db.session = session

    def teardown():
        session.close()
        # rollback - everything that happened with the
        # session above (including calls to commit())
        # is rolled back.
        transaction.rollback()
        # return connection to the Engine
        connection.close()

    request.addfinalizer(teardown)
    return session


@pytest.fixture(scope='function')
def request_ctx(app, request):
    ctx = app.test_request_context()
    ctx.push()

    def teardown():
        ctx.pop

    request.addfinalizer(teardown)
    return ctx


@pytest.fixture(scope='function')
def client(db, session, request, user_factory):
    u = user_factory(username='johndoe', password='password')
    session.add(u)
    session.commit()

    def teardown():
        session.rollback()

    request.addfinalizer(teardown)

    return TestClient(db.app, u.generate_auth_token(60), '')
