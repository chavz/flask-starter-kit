import re
import pytest

from werkzeug.exceptions import HTTPException

from app.users.models import Role


def test_get_roles(client):
    rv, json = client.get('/api/v1/roles')
    assert rv.status_code == 200
    assert json['data'] == []


def test_get_roles_with_filters(
        session, client, role_factory, privilege_factory):
    role_one = role_factory(name='one')
    role_two = role_factory(name='two')
    role_tri = role_factory(name='tri',  # noqa
        attach_privileges=[privilege_factory(name='for')])

    session.add_all([role_one, role_two, role_tri])
    session.commit()

    criterias = [
        ('name', 'o', 2), ('name', 't', 2),
        ('name', 'e', 1), ('name', 'r', 1),
        ('privileges', 'for', 1)]

    for field, val, total in criterias:
        rv, json = client.get(
            url='/api/v1/roles',
            params={'filter[' + field + ']': val})
        assert rv.status_code == 200
        assert len(json['data']) == total


def test_new_role(client):
    rv, json = client.post(
        url='/api/v1/roles', data={'data': {
            'type': 'roles', 'attributes': {'name': 'role_a'}}
        })
    assert rv.status_code == 201
    role_a = Role.query.filter_by(name='role_a').one()
    assert role_a is not None
    assert role_a.name == 'role_a'
    location = rv.headers['Location']
    assert re.compile(r'.*/roles/%s' % role_a.id).match(location,)


def test_new_role_with_data_empty_or_none(client):
    with pytest.raises(HTTPException):
        rv, json = client.post(url='/api/v1/roles', data=None)
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'
        rv, json = client.post(url='/api/v1/roles', data={})
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'


def test_get_role(session, client, role_factory):
    role_a = role_factory(name='role_a')
    session.add(role_a)
    session.commit()
    rv, json = client.get('/api/v1/roles/%s' % role_a.id)
    assert rv.status_code == 200
    assert json['data']['attributes']['name'] == 'role_a'


def test_get_role_with_none_existing_role(client):
    role_a = Role.query.filter_by(id='1').first()
    assert role_a is None
    with pytest.raises(HTTPException):
        rv, json = client.get('/api/v1/roles/1')
        assert rv.status_code == 404
        assert json['error'] == 'not found'
        assert json['message'] == 'item not found'


def test_update_role(session, client, role_factory):
    role_a = role_factory(name='is-a')
    session.add(role_a)
    session.commit()
    rv, json = client.put(
        url=('/api/v1/roles/%s' % role_a.id),
        data={'data': {
            'type': 'roles', 'id': role_a.id,
            'attributes': {'name': 'role_a'}}
        })
    assert rv.status_code == 200
    assert role_a.name, 'role_a'


def test_update_role_with_none_existing_role(client):
    role_a = Role.query.filter_by(id='1').first()
    assert role_a is None
    with pytest.raises(HTTPException):
        rv, json = client.put(
            url='/api/v1/roles/1',
            data={'name': 'is_b'})
        assert rv.status_code == 404
        assert json['error'] == 'not found'
        assert json['message'] == 'item not found'


def test_update_role_with_data_empty_or_none(
        session, client, role_factory):
    role_a = role_factory(name='is-a')
    session.add(role_a)
    session.commit()
    with pytest.raises(HTTPException):
        rv, json = client.put(
            url=('/api/v1/roles/%s' % role_a.id), data=None)
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'
        rv, json = client.put(
            url=('/api/v1/roles/%s' % role_a.id), data={})
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'


def test_get_role_privileges(
        session, client, role_factory, privilege_factory):
    is_a = privilege_factory(name='is_a')
    role_a = role_factory(name='role_a')
    role_a.privileges.append(is_a)
    session.add_all([is_a, role_a])
    session.commit()
    assert is_a in role_a.privileges
    rv, json = client.get('/api/v1/roles/%s/privileges' % role_a.id)
    assert rv.status_code == 200
    assert is_a.to_json()['data'] in json['data']


def test_get_role_privileges_with_filters(
        session, client, role_factory, privilege_factory):
    role_omni = role_factory(
        name='tri', attach_privileges=[
            privilege_factory(name='one'),
            privilege_factory(name='two'),
            privilege_factory(name='tri')
        ])

    session.add(role_omni)
    session.commit()

    criterias = [
        ('name', 'o', 2), ('name', 't', 2),
        ('name', 'e', 1), ('name', 'r', 1)]

    for field, val, total in criterias:
        rv, json = client.get(
            url='/api/v1/roles/%s/privileges' % role_omni.id,
            params={'filter[' + field + ']': val})
        assert rv.status_code == 200
        assert len(json['data']) == total


def test_assigning_privileges_to_roles(
        session, client, role_factory, privilege_factory):
    is_a = privilege_factory(name='is_a')
    role_a = role_factory(name='role_a')
    session.add_all([is_a, role_a])
    session.commit()

    rv, json = client.post(
        url='/api/v1/roles/%s/privileges' % role_a.id,
        data={'privileges': [is_a.name]})

    assert rv.status_code == 200
    assert is_a in role_a.privileges


def test_removing_privileges_from_roles(
        session, client, role_factory, privilege_factory):
    is_a = privilege_factory(name='is_a')
    role_a = role_factory(name='role_a')
    role_a.privileges.append(is_a)
    session.add_all([is_a, role_a])
    session.commit()
    assert is_a in role_a.privileges

    rv, json = client.delete(
        url='/api/v1/roles/%s/privileges' % role_a.id,
        data={'privileges': [is_a.name]})

    assert rv.status_code == 200
    assert is_a not in role_a.privileges
