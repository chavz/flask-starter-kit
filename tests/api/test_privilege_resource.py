import re
import pytest

from werkzeug.exceptions import HTTPException

from app.users.models import User, Privilege


def test_get_privileges(client):
    rv, json = client.get('/api/v1/privileges')
    assert rv.status_code == 200
    assert json['data'] == []


def test_get_privileges_with_filters(client, session, privilege_factory):
    priv_one = privilege_factory(name='one')
    priv_two = privilege_factory(name='two')
    priv_tri = privilege_factory(name='tri')

    session.add_all([priv_one, priv_two, priv_tri])
    session.commit()

    criterias = [
        ('name', 'o', 2), ('name', 't', 2),
        ('name', 'e', 1), ('name', 'r', 1)]

    for field, val, total in criterias:
        rv, json = client.get(
            url='/api/v1/privileges',
            params={'filter[' + field + ']': val})
        assert rv.status_code == 200
        assert len(json['data']) == total


def test_new_privilege(client):
    rv, json = client.post(
        url='/api/v1/privileges',
        data={'data': {
            'type': 'privileges', 'attributes': {'name': 'is_a'}}
        })
    assert rv.status_code == 201
    is_a = Privilege.query.filter_by(name='is_a').one()
    assert is_a is not None
    assert is_a.name == 'is_a'
    location = rv.headers['Location']
    assert re.compile(r'.*/privilege/%s' % is_a.id).match(location, )


def test_new_privilege_with_data_empty_or_none(client):
    with pytest.raises(HTTPException):
        rv, json = client.post(url='/api/v1/privileges', data=None)
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'
        rv, json = client.post(url='/api/v1/privileges', data={})
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'


def test_get_privilege(session, client, privilege_factory):
    is_a = privilege_factory(name='is_a')
    session.add(is_a)
    session.commit()
    rv, json = client.get('/api/v1/privilege/%s' % is_a.id)
    assert rv.status_code == 200
    assert json['data']['attributes']['name'] == 'is_a'


def test_get_privilege_with_none_existing_privilege(client):
    is_a = Privilege.query.filter_by(id='1').first()
    assert is_a is None
    with pytest.raises(HTTPException):
        rv, json = client.get('/api/v1/privilege/1')
        assert rv.status_code == 404
        assert json['error'] == 'not found'
        assert json['message'] == 'item not found'


def test_update_privilege(session, client, privilege_factory):
    is_a = privilege_factory(name='is-a')
    session.add(is_a)
    session.commit()
    rv, json = client.put(
        url=('/api/v1/privilege/%s' % is_a.id),
        data={'data': {'type': 'privileges', 'id': is_a.id,
                       'attributes': {'name': 'is_a'}}})
    assert rv.status_code == 200
    assert is_a.name == 'is_a'


def test_update_privilege_with_none_existing_privilege(client):
    is_a = Privilege.query.filter_by(id='1').first()
    assert is_a is None
    with pytest.raises(HTTPException):
        rv, json = client.put(
            url='/api/v1/privilege/1',
            data={'name': 'is_b'})
        assert rv.status_code == 404
        assert json['error'] == 'not found'
        assert json['message'] == 'item not found'


def test_update_privilege_with_data_empty_or_none(
        session, client, privilege_factory):
    is_a = privilege_factory(name='is-a')
    session.add(is_a)
    session.commit()
    with pytest.raises(HTTPException):
        rv, json = client.put(
            url=('/api/v1/privilege/%s' % is_a.id), data=None)
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'
        rv, json = client.put(
            url=('/api/v1/privilege/%s' % is_a.id), data={})
        assert rv.status_code == 400
        assert json['error'] == 'bad request'
        assert json['message'] == 'No data submitted'


def test_privilege_map(session, client, privilege_factory):
    is_a = privilege_factory(name='is_a')
    is_b = privilege_factory(name='is_b')
    is_c = privilege_factory(name='is_c')
    session.add_all([is_a, is_b, is_c])
    session.commit()
    u = User.query.filter_by(username='johndoe').one()
    u.add_user_privileges('is_a', 'is_b')
    session.commit()
    rv, json = client.post(
        url='/api/v1/user/privilege-map',
        data={'privilegeList': ['is_a', 'is_b', 'is_c']})
    assert rv.status_code == 200
    assert (
        json['privilegeMap'] == {'is_a': True, 'is_b': True, 'is_c': False}
    )
