import factory
from faker import Factory as FakerFactory


class ModelFactory(factory.Factory):
    class Meta:
        abstract = True
        strategy = factory.BUILD_STRATEGY

faker = FakerFactory.create()

# flake8: noqa
from .core import *
from .users import *

