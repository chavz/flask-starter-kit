import factory
from pytest_factoryboy import register

from tests.factories import ModelFactory, faker
from tests.factories.core import PartyFactory

from app.users import models


@register
class UserFactory(ModelFactory):
    class Meta:
        model = models.User

    username = factory.Sequence(lambda n: u'User %d' % n)
    password = faker.password(length=10)

    @factory.post_generation
    def attach_owner(self, create, extracted, **kwargs):
        self.owner = PartyFactory()


@register
class PrivilegeFactory(ModelFactory):
    class Meta:
        model = models.Privilege

    name = factory.Sequence(lambda n: u'Privilege %d' % n)


@register
class RoleFactory(ModelFactory):
    class Meta:
        model = models.Role

    name = factory.Sequence(lambda n: u'Role %d' % n)

    @ factory.post_generation
    def attach_privileges(self, create, extracted, **kwargs):
        if not extracted:
            return
        elif isinstance(extracted, list):
            self.privileges = extracted
        else:
            total, i = kwargs.get('size', 5), 0
            while i < total:
                self.privileges.append(PrivilegeFactory())
