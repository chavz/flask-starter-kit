import factory

from factory import fuzzy
from pytest_factoryboy import register

from tests.factories import ModelFactory
from app.core import models, enums


@register
class PartyFactory(ModelFactory):
    class Meta:
        model = models.Party

    name = factory.Sequence(lambda n: u'Party %d' % n)
    type_ = fuzzy.FuzzyChoice([type_ for type_ in enums.PartyType])
