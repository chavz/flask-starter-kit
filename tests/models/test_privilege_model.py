import pytest
import re

from app.exceptions import ValidationError
from app.users.models import Privilege


def test_get_url(session):
    p = Privilege(name='is_a')
    session.add(p)
    session.commit()
    assert re.compile(r'.*/privilege/%s' % p.id).match(p.get_url())


def test_to_json(session):
    p = Privilege(name='is_a')
    session.add(p)
    session.commit()
    json = {
        'links': {'self': '/privileges/%s' % p.id},
        'data': {
            'id': p.id, 'type': 'privileges',
            'attributes': {'name': p.name},
            'links': {'self': '/privileges/%s' % p.id}
        }
    }
    assert json == p.to_json()


def test_add_from_json():
    json = {'name': 'is_a'}
    p = Privilege().add_from_json(json)
    assert isinstance(p, Privilege)
    assert p.name == 'is_a'


def test_add_from_json_with_data_none_or_empty():
    with pytest.raises(ValidationError):
        Privilege().add_from_json(None)
    with pytest.raises(ValidationError):
        Privilege().add_from_json({})


def test_update_from_json(session):
    p = Privilege(name='isab')
    session.add(p)
    session.commit()
    assert p.name == 'isab'
    p.update_from_json({'name': 'is_a'})
    assert p.name == 'is_a'


def test_repr():
    p = Privilege(name='is_a')
    rep = '<Privilege (id=%r, name=%r)>' % (p.id, p.name)
    assert rep == p.__repr__()
