import pytest
import time

from app.exceptions import ValidationError


def test_password_setter(user_factory):
    u = user_factory(password='cat')
    assert u.password_hash is not None


# noinspection PyStatementEffect
def test_no_password_getter(user_factory):
    u = user_factory(password='cat')
    with pytest.raises(AttributeError) as excinfo:
        u.password
    assert excinfo.value.message == 'password is not a readable attribute'


def test_password_verification(user_factory):
    u = user_factory(password='cat')
    assert u.verify_password('cat') is True
    assert u.verify_password('dog') is False


def test_password_salts_are_random(user_factory):
    u = user_factory(password='cat')
    u2 = user_factory(password='cat')
    assert u.password_hash != u2.password_hash


def test_valid_authentication_token(session, user_factory):
    u = user_factory()
    session.add(u)
    session.commit()
    token = u.generate_auth_token(expiration=300)
    assert u.verify_auth_token(token) is not None


def test_invalid_authentication_token(session, user_factory):
    u1 = user_factory()
    u2 = user_factory()
    session.add_all([u1, u2])
    session.commit()
    token = u1.generate_auth_token(expiration=300)
    assert u2.verify_auth_token(token) is not None


def test_expired_authentication_token(session, user_factory):
    u = user_factory()
    session.add(u)
    session.commit()
    token = u.generate_auth_token(expiration=1)
    time.sleep(2)
    assert u.verify_auth_token(token) is None


def test_add_user_privilege(session, user_factory, privilege_factory):
    u = user_factory()
    p = privilege_factory(name='is_a')
    session.add_all([u, p])
    session.commit()
    u.add_user_privileges('is_a')
    assert p in u.user_privileges


def test_add_user_privilege_with_none_existing_privilege(
        session, user_factory):
    u = user_factory()
    session.add(u)
    session.commit()
    with pytest.raises(ValidationError):
        u.add_user_privileges('is_a')


def test_add_user_privilege_with_already_owned_privilege(
        session, user_factory, privilege_factory):
    u = user_factory()
    p = privilege_factory(name='is_a')
    session.add_all([u, p])
    session.commit()
    u.add_user_privileges('is_a')
    session.commit()
    u.add_user_privileges('is_a')
    assert len(u.user_privileges) < 2
    assert p == u.user_privileges[0]


def test_remove_user_privilege(session, user_factory, privilege_factory):
    u = user_factory()
    is_a = privilege_factory(name='is_a')
    session.add_all([u, is_a])
    session.commit()
    u.add_user_privileges('is_a')
    assert is_a in u.user_privileges
    u.remove_user_privileges('is_a')
    assert is_a not in u.user_privileges


def test_remove_user_privilege_with_not_owned_privilege(
        session, user_factory, privilege_factory):
    u = user_factory()
    is_a = privilege_factory(name='is_a')
    is_b = privilege_factory(name='is_b')
    session.add_all([u, is_a, is_b])
    session.commit()
    u.add_user_privileges('is_a')
    assert is_a in u.user_privileges
    u.remove_user_privileges('is_b')
    assert is_a in u.user_privileges
    assert len(u.user_privileges) < 2


def test_add_user_role(session, user_factory, role_factory):
    u = user_factory()
    r = role_factory()
    session.add_all([u, r])
    session.commit()
    u.add_user_roles(r.name)
    assert r in u.user_roles


def test_add_user_role_with_none_existing_role(session, user_factory):
    u = user_factory()
    session.add(u)
    session.commit()
    with pytest.raises(ValidationError):
        u.add_user_roles('Admin')


def test_add_user_role_with_already_owned_role(
        session, user_factory, role_factory):
    u = user_factory()
    r = role_factory()
    session.add_all([u, r])
    session.commit()
    u.add_user_roles(r.name)
    session.commit()
    assert r in u.user_roles
    u.add_user_roles(r.name)
    assert len(u.user_roles) < 2
    assert r == u.user_roles[0]


def test_remove_user_role(session, user_factory, role_factory):
    u = user_factory()
    r = role_factory()
    session.add_all([u, r])
    session.commit()
    u.add_user_roles(r.name)
    assert r in u.user_roles
    u.remove_user_roles(r.name)
    assert r not in u.user_roles


def test_remove_user_role_with_not_owned_role(
        session, user_factory, role_factory):
    u = user_factory()
    r_owned = role_factory()
    r_not_owned = role_factory()
    session.add_all([u, r_owned, r_not_owned])
    session.commit()
    u.add_user_roles(r_owned.name)
    assert r_owned in u.user_roles
    u.remove_user_roles(r_not_owned.name)
    assert r_owned in u.user_roles
    assert len(u.user_roles) < 2


def test_privileges_property_returns_all_owned_privileges(
        session, user_factory, privilege_factory, role_factory):
    u = user_factory()
    session.add(u)
    is_a = privilege_factory(name='is_a')
    is_b = privilege_factory(name='is_b')
    is_c = privilege_factory(name='is_c')
    session.add(is_a)
    session.add(is_b)
    session.add(is_c)
    r = role_factory(name='R')
    r.add_privileges('is_b')
    session.add(r)
    session.commit()
    u.add_user_roles('R')
    u.add_user_privileges('is_a')
    assert is_a in u.privileges
    assert is_b in u.privileges
    assert is_c not in u.privileges


def test_privilege_map(session, user_factory, privilege_factory):
    u = user_factory()
    is_a = privilege_factory(name='is_a')
    is_b = privilege_factory(name='is_b')
    is_c = privilege_factory(name='is_c')
    session.add_all([u, is_a, is_b, is_c])
    session.commit()
    u.add_user_privileges('is_a', 'is_b')
    priv_map = {u'is_a': True, u'is_b': True, u'is_c': False}
    assert priv_map == u.privilege_map('is_a', 'is_b', 'is_c')


def test_repr(user_factory):
    u = user_factory()
    assert (
        '<User (party_id=%r, username=%r)>'
        % (u.party_id, u.username) == u.__repr__()
    )
