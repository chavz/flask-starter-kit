import pytest
import re

from app.exceptions import ValidationError
from app.users.models import Role, Privilege


def test_get_url(session):
    r = Role(name='Role1')
    session.add(r)
    session.flush()
    assert re.compile(r'.*/roles/%s' % r.id).match(r.get_url())


def test_to_json_with_expanded_true(session):
    r = Role(name='Role1')
    session.add(r)
    p = Privilege(name='is_a')
    session.add(p)
    session.flush()
    r.add_privileges('is_a')
    session.flush()
    rjson = r.to_json(expanded=True)
    assert rjson['data']['attributes']['name'] == 'Role1'
    assert rjson['links']['self'] == '/roles/%s' % r.id
    assert len(rjson['data']['relationships']['privileges']['data']) == 1
    assert (
        rjson['data']['relationships']
        ['privileges']['data'][0]['id'] == str(p.id)
    )


def test_add_from_json():
    json = {'name': 'Role1'}
    r = Role().add_from_json(json)
    assert isinstance(r, Role)
    assert r.name == 'Role1'


def test_add_from_json_with_data_none_or_empty():
    with pytest.raises(ValidationError):
        Role().add_from_json(None)
    with pytest.raises(ValidationError):
        Role().add_from_json({})


def test_update_from_json(session):
    r = Role(name='Rle1')
    session.add(r)
    session.flush()
    assert r.name == 'Rle1'
    r.update_from_json({'name': 'Role1'})
    assert r.name == 'Role1'


def test_add_privileges(session):
    r = Role(name='Role1')
    session.add(r)
    p = Privilege(name='is_a')
    session.add(p)
    r.add_privileges('is_a')
    session.flush()
    assert p in r.privileges


def test_add_privileges_with_none_existing_privilege(session):
    r = Role(name='Role1')
    session.add(r)
    session.flush()
    with pytest.raises(ValidationError):
        r.add_privileges('is_a')


def test_add_privileges_with_already_owned_privilege(session):
    r = Role(name='Role1')
    session.add(r)
    p = Privilege(name='is_a')
    session.add(p)
    r.add_privileges('is_a')
    session.flush()
    r.add_privileges('is_a')
    assert len(r.privileges) == 1
    assert p == r.privileges[0]


def test_remove_privileges(session):
    r = Role(name='Role1')
    session.add(r)
    p = Privilege(name='is_a')
    session.add(p)
    session.flush()
    r.add_privileges('is_a')
    assert p in r.privileges
    r.remove_privileges('is_a')
    assert p not in r.privileges


def test_remove_privileges_with_not_owned_privilege(session):
    r = Role(name='Role1')
    session.add(r)
    is_a = Privilege(name='is_a')
    session.add(is_a)
    is_b = Privilege(name='is_b')
    session.add(is_b)
    session.flush()
    r.add_privileges('is_a')
    assert is_a in r.privileges
    assert is_b not in r.privileges
    assert len(r.privileges) == 1
    r.remove_privileges('is_b')
    assert is_a in r.privileges
    assert is_b not in r.privileges
    assert len(r.privileges) == 1


def test_repr():
    r = Role(name='Role1')
    assert '<Role (id=%r, name=%r)>' % (r.id, r.name) == r.__repr__()
