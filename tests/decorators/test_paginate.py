from flask import json
from app.core.decorators import paginate
from app.users.schemas import PrivilegeSchema


# noinspection PyUnusedLocal
def test_paginating_iterable(mocker, request_ctx, privilege_factory):
    mocker.patch('app.core.decorators.paginate.url_for', return_value='/')

    schema = PrivilegeSchema
    iterable = [privilege_factory() for p in range(0, 100)]
    params = {'paginate': True, 'page': 1, 'count': 10}

    # noinspection PyUnusedLocal
    @paginate.iter(schema)
    def view_func(args):
        return iterable

    data = json.loads(view_func(params).data)

    # all the required keys for pagination must be present
    assert all(key in data.keys() for key in ['prev', 'next', 'first', 'last'])
    # top-level links must be present as well
    assert data['links'] and data['links']['self']
    # check correctness of the returned data
    assert schema().load({'data': data['data'][0]})
    # check correctness of additional keys
    assert data['total'] == len(iterable)
    assert data['pages'] == len(iterable) / params['count']

    params['paginate'] = False
    data = json.loads(view_func(params).data)

    # since no pagination requested, additional keys
    # added for pagination must not be present
    assert all(
        key not in data.keys()
        for key in ['prev', 'next', 'first', 'last'])
    assert data.get('pages') is None
    # "total" key still stands
    assert data['total'] == len(iterable)
    # "data" now contains the full collection retrieved
    assert len(data['data']) == data['total']
    # and each object it contains should follow the schema
    assert schema().load({'data': data['data'][0]})
    # top-level links must still be present as well
    assert data['links'] and data['links']['self']


# noinspection PyUnusedLocal
def test_paginating_query(mocker, request_ctx, session, privilege_factory):
    mocker.patch('app.core.decorators.paginate.url_for', return_value='/')

    # noinspection PyProtectedMember
    query = privilege_factory._meta.model.query
    schema = PrivilegeSchema

    items_count = 25
    params = {'paginate': True, 'page': 1, 'count': 10}

    session.add_all([privilege_factory() for p in range(0, items_count)])
    session.commit()

    # noinspection PyUnusedLocal
    @paginate.query(schema)
    def view_func(args):
        return query

    data = json.loads(view_func(params).data)

    # all the required keys for pagination must be present
    assert all(key in data.keys() for key in ['prev', 'next', 'first', 'last'])
    # top-level links must be present as well
    assert data['links'] and data['links']['self']
    # check correctness of the returned data
    assert schema().load({'data': data['data'][0]})
    # check correctness of additional keys
    assert data['total'] == items_count
    assert data['pages'] == (
        (items_count + params['count'] // 2) // params['count'])

    # send another request, this time disabling pagination
    params['paginate'] = False
    data = json.loads(view_func(params).data)
    # since no pagination requested, additional keys
    # added for pagination must not be present
    assert all(
        key not in data.keys() for
        key in ['prev', 'next', 'first', 'last'])
    assert data.get('pages') is None
    # "total" key still stands
    assert data['total'] == items_count
    # "data" now contains the full collection retrieved
    assert len(data['data']) == data['total']
    # and each object it contains should follow the schema
    assert schema().load({'data': data['data'][0]})
    # top-level links must still be present as well
    assert data['links'] and data['links']['self']
