-r common.txt
factory-boy==2.8.0
mock==2.0.0
pytest==3.2.0
pytest-cov==2.3.1
pytest-factoryboy==1.2.2
pytest-flake8==0.9
pytest-mock==1.2
