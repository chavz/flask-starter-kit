#!/usr/bin/env python

"""This module launches the application and other application tasks."""

import os

# look for the environment configuration file,
# if it exists, import them as environment variables
if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from flask_script import Manager, Shell
from flask_script.commands import ShowUrls, Clean
from flask_migrate import Migrate, MigrateCommand

from app import create_app, db

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    """Return objects that will be available in the shell."""
    return dict(app=app, db=db)


manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)
manager.add_command("show-urls", ShowUrls())
manager.add_command("clean", Clean())


if __name__ == '__main__':
    manager.run()
