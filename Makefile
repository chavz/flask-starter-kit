.PHONY: docs test

help:
	@echo "  clean       remove unwanted files like .pyc's"
	@echo "  lint        check style with flake8"
	@echo "  test        run all your tests using py.test"
	@echo "  run         startup the werkzeug server at port 5000"

clean:
	python manage.py clean

lint:
	flake8 --exclude=venv .

test:
	py.test  --verbose --flake8 \
             --cov-report term-missing --cov-report html  --cov  app

run:
	python manage.py runserver --host 0.0.0.0 --port 5000 --threaded
