"""This module stores the configuration settings.

Attributes:
  basedir (str): absolute path of the application base directory.

"""
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:

    """Common configurations."""

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'

    DEFAULT_PAGE_SIZE = int(os.environ.get('DEFAULT_PAGE_SIZE') or 10)
    AUTH_TOKEN_EXPIRATION = int(os.environ.get('AUTH_TOKEN_EXPIRATION') or 3200)

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):

    """Development specific configurations."""

    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'mysql+pymysql://root:password@localhost/app_dev_db'


class TestingConfig(Config):

    """Testing specific configurations."""

    TESTING = True
    SERVER_NAME = 'localhost'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'mysql+pymysql://root:password@mysql/app_test_db'


class ProductionConfig(Config):

    """Production specific configurations."""
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'mysql+pymysql://root:password@localhost/app_prod_db'


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}
